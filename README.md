# gnome-shell-extension-topicons-plus-git

# DISCONTINUED - Instead, use gnome-shell-extension-appindicator

Shows legacy tray icons on top.

https://github.com/phocean/TopIcons-plus

https://extensions.gnome.org/extension/1031/topicons/

How to clone this repo:

```
git clone https://gitlab.com/reborn-os-team/gnome-shell-extensions/gnome-shell-extension-topicons-plus-git.git
```

